#include <iostream>
#include "myc.h"
#include "identical.h" // where is it coming from mya or myb?

void myc(){
    #ifdef NDEBUG
    std::cout << "Hello World Release from MyC!" <<std::endl;
    #else
    std::cout << "Hello World Debug from MyC!" <<std::endl;
    #endif
    std::cout << "Identical header file included from " << INCLUDED_FROM << std::endl;
}
